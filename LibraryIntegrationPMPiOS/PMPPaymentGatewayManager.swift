//
//  PMPPaymentGatewayManager.swift
//  LibraryIntegrationPMPiOS
//
//  Created by APPLE on 17/09/17.
//  Copyright © 2017 LUIS MARCA ZAPATA. All rights reserved.
//

import UIKit

//**********************************************************************************************************
//
// MARK: - Constants -
//
//**********************************************************************************************************

let kGlobalEnvironmentGateway: Bool = true
//**********************************************************************************************************
//
// MARK: - Definitions -
//
//**********************************************************************************************************

enum CardType: String {
    case MASTERCARD = "master_icon"
}

class PMPPaymentGatewayManager {

    
    //**************************************************
    // MARK: - Internal Methods
    //**************************************************
    
    private class func showUserAlertForErrorOnGateway(_ view: UIViewController, _ isPrefScreen: Bool) {
        let alertView: UIAlertController = UIAlertController(title: "Alerta",
                                                             message: "En estos momentos no se puede procesar tu pago.\nReintenta más tarde.",
                                                             preferredStyle: .alert)
        let okButton: UIAlertAction = UIAlertAction(title: "OK",
                                                    style: .default,
                                                    handler: {
                                                        (action) in
                                                        print("error")
        })
        alertView.addAction(okButton)
        view.present(alertView, animated: true, completion: nil)
    }
    
    //**************************************************
    // MARK: - Self Public Methods
    //**************************************************
    class func payment(view: UIViewController,
                       brand: CardType = .MASTERCARD,
                       user: PMPUser,
                       isPrefScreen: Bool = false) {
        
        switch brand {
        case .MASTERCARD:
            let creditCard = PMPMCTransaction(user: user, isPrefScreen: isPrefScreen)
            print("Cargando parámetros")
            print("Operation: Credit Card MC - loading parameters")
            if creditCard.loadParameters() {
                print("Conectando MC")
                print("Operation: Credit Card MC - Calling brand")
                creditCard.callPayment(view: view)
            } else {
                self.showUserAlertForErrorOnGateway(view, isPrefScreen)
            }
        }
    }
    
    
    
}
