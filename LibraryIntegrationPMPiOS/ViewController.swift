//
//  ViewController.swift
//  LibraryIntegrationPMPiOS
//
//  Created by APPLE on 17/09/17.
//  Copyright © 2017 LUIS MARCA ZAPATA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func OnClick(_ sender: UIButton) {
        
        let user = PMPUser(email: "lmarca@mc.com.pe",
                           name: "Luis",
                           firstName: "Marca",
                           documentNumber: "44906618",
                           amount: 12.50)
        
        PMPPaymentGatewayManager.payment(view: self, brand: .MASTERCARD, user: user, isPrefScreen: true)
    }
    

}

