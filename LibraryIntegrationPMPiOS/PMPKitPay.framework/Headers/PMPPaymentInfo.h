//
//  PMPPaymentInfo.h
//  LibPayPMP
//
//  Created by Procesos on 13/06/16.
//  Copyright © 2016 PMP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMPPaymentInfo : NSObject

@property(nonatomic, copy, readwrite) NSString *TransactionCardBrand;
@property(nonatomic, copy, readwrite) NSString *TransactionResult;
@property(nonatomic, copy, readwrite) NSString *MerchantCode;
@property(nonatomic, copy, readwrite) NSString *BuyerTokenId;
@property(nonatomic, copy, readwrite) NSString *TransactionOrderNumber;
@property(nonatomic, copy, readwrite) NSString *TransactionCurrency;
@property(nonatomic, copy, readwrite) NSString *TransactionAmount;
@property(nonatomic, copy, readwrite) NSString *TransactionReferenceNumber;
@property(nonatomic, copy, readwrite) NSString *TransactionAuthorizationCode;
@property(nonatomic, copy, readwrite) NSString *TransactionCard;
@property(nonatomic, copy, readwrite) NSString *TransactionCountry;
@property(nonatomic, copy, readwrite) NSString *TransactionCuotasNumber;
@property(nonatomic, copy, readwrite) NSString *TransactionFirstCuotaDate;
@property(nonatomic, copy, readwrite) NSString *TransactionCuotaCurrency;
@property(nonatomic, copy, readwrite) NSString *TransactionCuotaAmount;
@property(nonatomic, copy, readwrite) NSString *TransactionId;
@property(nonatomic, copy, readwrite) NSDictionary *data; // nuevo atributo
@property(nonatomic, copy, readwrite) NSString *TransactionAuthorizationDate;
@property(nonatomic, copy, readwrite) NSString *TransactionAuthorizationTime;
@property(nonatomic, copy, readwrite) NSString *NUMBER_ATTEMPT_DENIED_DAY;
@property(nonatomic, copy, readwrite) NSString *NUMBER_ATTEMPT_APPROVED_DAY;
@property(nonatomic, copy, readwrite) NSString *ResponseCode;
@property(nonatomic, copy, readwrite) NSString *ResponseMessage;


- (instancetype) initWithTransactionCardBrand:(NSString *)aTransactionCardBrand transactionResult:(NSString *)aTransactionResult merchantCode:(NSString *)aMerchantCode buyerTokenId:(NSString *)aBuyerTokenId transactionOrderNumber:(NSString *)aTransactionOrderNumber transactionCurrency:(NSString *)aTransactionCurrency transactionAmount:(NSString *)aTransactionAmount transactionReferenceNumber:(NSString *)aTransactionReferenceNumber transactionAuthorizationCode:(NSString *)aTransactionAuthorizationCode transactionCard:(NSString *)aTransactionCard transactionCountry:(NSString *)aTransactionCountry transactionCuotasNumber:(NSString *)aTransactionCuotasNumber transactionFirstCuotaDate:(NSString *)aTransactionFirstCuotaDate transactionCuotaCurrency:(NSString *)aTransactionCuotaCurrency transactionCuotaAmount:(NSString *)aTransactionCuotaAmount transactionId:(NSString *)aTransactionId transactionAuthorizationDate:(NSString *)aTransactionAuthorizationDate transactionAuthorizationTime:(NSString *)aTransactionAuthorizationTime responseCode:(NSString *)aResponseCode responseMessage:(NSString *)aResponseMessage data:(NSDictionary *)aData;



@end
