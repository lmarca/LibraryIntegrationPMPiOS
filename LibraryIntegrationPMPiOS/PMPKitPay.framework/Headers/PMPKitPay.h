//
//  PMPKitPay.h
//  PMPKitPay
//
//  Created by Procesos on 15/06/16.
//  Copyright © 2016 PMP. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PMPKitPay.
FOUNDATION_EXPORT double PMPKitPayVersionNumber;

//! Project version string for PMPKitPay.
FOUNDATION_EXPORT const unsigned char PMPKitPayVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PMPKitPay/PublicHeader.h>

#import <PMPKitPay/PayerInitVC.h>
#import <PMPKitPay/PMPConfigurationContext.h>
#import <PMPKitPay/PMPPaymentInfo.h>
#import <PMPKitPay/PMPPaymentPresentAnimationController.h>
#import <PMPKitPay/PMPPaymentDismissAnimationController.h>
