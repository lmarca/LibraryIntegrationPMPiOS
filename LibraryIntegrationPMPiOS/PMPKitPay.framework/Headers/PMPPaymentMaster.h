//
//  PMPPaymentMaster.h
//  PMPKitPay
//
//  Created by Procesos on 23/06/16.
//  Copyright © 2016 PMP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMPConfigurationContext.h"
#import "PMPPaymentInfo.h"
#import "PMPPaymentDelegate.h"

@interface PMPPaymentMaster : UIViewController
{
    id<PMPPaymentDelegate> _delegate;
}

@property(nonatomic, strong) id<PMPPaymentDelegate> delegate;

- (UIStoryboard *)mainStoryBoard;
- (void)show:(NSString *)Class;
- (void)LoadingOn:(NSString *)msj;
- (void)LoadingOff;
- (void)paymentDidCancel;
- (void)showToast:(NSString *)msj;
- (void) roundingCornersView : (UIView *)view;
- (void)padding:(NSInteger)padding textField:(UITextField *)textField thumb:(NSString *)thumb isLeft:(BOOL)isLeft;
- (void)sendData:(NSString *)ResponseCode ResponseMessage:(NSString *)ResponseMessage;

@end
