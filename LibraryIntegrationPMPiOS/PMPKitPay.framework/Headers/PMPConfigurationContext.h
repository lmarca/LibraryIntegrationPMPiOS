//
//  PMPConfigurationContext.h
//  LibPayPMP
//
//  Created by Procesos on 13/06/16.
//  Copyright © 2016 PMP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMPConfigurationContext : NSObject

typedef NS_ENUM(NSInteger, PAYMENT_ENVIRONMENT) {
    PRODUCTION = 1,
    DEVELOPMENT = 2
};

typedef NS_ENUM(NSInteger, PAYMENT_EVENT) {
    AUTORIZACION = 1,
    REGISTRAR_TARJETA = 2
};


typedef NS_ENUM(NSInteger, PMPLanguage) {
    DEFAULT = 0,
    SPANISH = 1,
    ENGLISH = 2
};

typedef NS_ENUM(NSInteger, PMPCurrency) {
    PEN = 604,
    USD = 840
};


@property(nonatomic, copy, readwrite) NSString *ApiLogin;
@property(nonatomic, copy, readwrite) NSString *ApiKey;
@property(nonatomic, copy, readwrite) NSString *MerchantCode;
@property(nonatomic, copy, readwrite) NSString *TransactionOrderNumber;
@property(nonatomic, assign, readwrite) PMPLanguage TransactionLanguage;
@property(nonatomic, assign, readwrite) PAYMENT_EVENT event;
@property(nonatomic, assign, readwrite) PAYMENT_ENVIRONMENT entorno;
@property(nonatomic, readwrite) double TransactionAmount;
@property(nonatomic, copy, readwrite) NSString *TransactionMerchantCode;
@property(nonatomic, copy, readwrite) NSString *TransactionSignature;
@property(nonatomic, assign, readwrite) PMPCurrency TransactionCurrency;
@property(nonatomic, copy, readwrite) NSString *BuyerTokenId;
@property(nonatomic, copy, readwrite) NSString *BuyerMerchantId;
@property(nonatomic, copy, readwrite) NSString *BuyerDocType;
@property(nonatomic, copy, readwrite) NSString *BuyerDocNumber;
@property(nonatomic, copy, readwrite) NSString *BuyerFirstName;
@property(nonatomic, copy, readwrite) NSString *BuyerLastName;
@property(nonatomic, copy, readwrite) NSString *BuyerEmail;
@property(nonatomic, copy, readwrite) NSString *TransactionAutogenerate;
@property(nonatomic, copy, readwrite) NSString *TransactionAutenticationDate;
@property(nonatomic, copy, readwrite) NSString *TransactionAutenticationTime;


+ (PMPConfigurationContext *) sharedInstance;
- (void) initializationData;

- (instancetype) initWithAutenticationApiLogin:(NSString *)aApiLogin apiKey:(NSString *)aApiKey merchantCode:(NSString *)aMerchantCode
                        transactionOrderNumber:(NSString *)aTransactionOrderNumber transactionCurrency:(PMPCurrency )aTransactionCurrency transactionAmount:(double )aTransactionAmount transactionMerchantCode:(NSString *)aTransactionMerchantCode transactionSignature:(NSString *)aTransactionSignature transactionLanguage:(PMPLanguage )aTransactionLanguage buyerTokenId:(NSString *)aBuyerTokenId buyerMerchantId:(NSString *)aBuyerMerchantId buyerDocType:(NSString *)aBuyerDocType buyerDocNumber:(NSString *)aBuyerDocNumber buyerFirstName:(NSString *)aBuyerFirstName buyerLastName:(NSString *)aBuyerLastName buyerEmail:(NSString *)aBuyerEmail transactionAutogenerate:(NSString *)aTransactionAutogenerate transactionAutenticationDate:(NSString *)aTransactionAutenticationDate transactionAutenticationTime:(NSString *)aTransactionAutenticationTime event:(PAYMENT_EVENT )aEvent entorno:(PAYMENT_ENVIRONMENT )aEntorno;

+ (NSString *)HmacSha1:(NSString *)key data:(NSString *)data;


@end
