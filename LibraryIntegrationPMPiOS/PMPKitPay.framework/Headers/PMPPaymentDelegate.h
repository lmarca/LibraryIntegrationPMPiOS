//
//  PMPPaymentDelegate.h
//  PMPKitPay
//
//  Created by Procesos on 16/06/16.
//  Copyright © 2016 PMP. All rights reserved.
//

#ifndef PMPPaymentDelegate_h
#define PMPPaymentDelegate_h

@protocol PMPPaymentDelegate <NSObject>
@required

- (void) paymentDidCancel;
- (void) paymendDidComplete: (PMPPaymentInfo *) paymentInfo;

@end


#endif /* PMPPaymentDelegate_h */
