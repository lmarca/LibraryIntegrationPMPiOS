//
//  PayerInitVC.h
//  LibPayPMP
//
//  Created by Procesos on 10/06/16.
//  Copyright © 2016 PMP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMPPaymentMaster.h"
#import "PMPConfigurationContext.h"

@interface PayerInitVC : PMPPaymentMaster

- (id)initWithContext:(PMPConfigurationContext *)ctx;


@end
