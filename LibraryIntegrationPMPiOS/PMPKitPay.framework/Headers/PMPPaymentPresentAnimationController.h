//
//  PMPPaymentPresentAnimationController.h
//  PMPKitPay
//
//  Created by Procesos on 23/06/16.
//  Copyright © 2016 PMP. All rights reserved.
//

@import UIKit;

@interface PMPPaymentPresentAnimationController : NSObject <UIViewControllerAnimatedTransitioning>

@end
