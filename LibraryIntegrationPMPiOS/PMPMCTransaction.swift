//
//  PMPMCTransaction.swift
//  LibraryIntegrationPMPiOS
//
//  Created by APPLE on 17/09/17.
//  Copyright © 2017 LUIS MARCA ZAPATA. All rights reserved.
//

import PMPKitPay

//**********************************************************************************************************
//
// MARK: - Constants -
//
//**********************************************************************************************************

//**********************************************************************************************************
//
// MARK: - Definitions -
//
//**********************************************************************************************************

enum MCErrorType {
    case NUMERICO, ALFANUMERICO, SIN_ERROR
}

//**********************************************************************************************************
//
// MARK: - Class -
//
//**********************************************************************************************************


class PMPMCTransaction: NSObject {
    
    //**************************************************
    // MARK: - Properties
    //**************************************************
    
    var cardholder: PMPUser
    var cardholderEmail: String
    var cardholderName: String
    var cardholderLastName: String
    var cardholderKindDoc: String = "OTROS"
    
    var transactionAmount: Double
    var transactionDate: String
    var transactionTime: String
    var transactionOrderNumber: String
    var isTransactionRegister: Bool = false
    
    var isPrefScreen: Bool
    // Gateway properties
    var tokenDevice: String?
    var userDeviceCode: String?
    
    var kindTransaction: String = "**Registro**"
    
    var viewControllerContainer: UIViewController?
    var gatewayContext: PMPConfigurationContext?
    
    //**************************************************
    // MARK: - Constructors
    //**************************************************
    
    init (user: PMPUser,
          isPrefScreen: Bool = false) {
        
        self.cardholder = user
        self.cardholderName = ""
        self.cardholderEmail = user.email
        self.cardholderName = user.name
        self.cardholderLastName = user.firstName
        
        //self.transaction = transaction
        
        self.transactionAmount = 12.50
        self.transactionDate = "20160101"
        self.transactionTime = "010101"
        self.transactionOrderNumber = "SSSS"
        
        self.isPrefScreen = isPrefScreen
        
        self.kindTransaction = isPrefScreen ? "**Registro**" : "**Consumo**"
        self.tokenDevice = ""
        
    }
    
    //**************************************************
    // MARK: - Internal Methods
    //**************************************************
    
    //**************************************************
    // MARK: - Private Methods
    //**************************************************
    
    private func getSignature(mcContext: PMPConfigurationContext) -> String {
        
        //signature un--encripted = MerchantCode + TransactionOrderNumber + Currency + Amount + ApiKey
        let signatureUnEncripted = NSMutableString()
        signatureUnEncripted.append(mcContext.merchantCode)
        signatureUnEncripted.append(mcContext.transactionOrderNumber)
        signatureUnEncripted.append("PEN")
        signatureUnEncripted.append(String(format: "%.2f", mcContext.transactionAmount))
        signatureUnEncripted.append(mcContext.apiKey)
        
        print("Original signature: \(signatureUnEncripted)")
        
        return PMPConfigurationContext.hmacSha1(mcContext.apiKey, data: signatureUnEncripted as String)
    }
    
    private func getTransactionOrderNumber() -> String {
        let tran = Date()
        return String(format: "%.0f", tran.timeIntervalSince1970 * 1000)
         
    }
    
    private func getpaymentDate(stringFromDateFormat: String) -> String {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = stringFromDateFormat
    
        return formatter.string(from: date)
    }
    
    
    //**************************************************
    // MARK: - Self Public Methods
    //**************************************************
    
    func loadMerchantVariables() throws -> PMPConfigurationContext {
        let mcContext = PMPConfigurationContext()
        
        //        guard let tx = self.transaction, let dealerID = tx.dealerID,
        //            let dealer = PADealer.fetch(key: dealerID),
        //            let MC_merchantID = dealer.merchantCodeMC,
        //            let MC_accesKeyID = dealer.apiKeyMC,
        //            let MC_secretAccessKey = dealer.secretKeyMC else {
        //                throw NSError(domain: "No se encuentran parametros de acceso a pasarela MC", code: 0, userInfo: nil)
        //        }
        //
        //        if let subMerchant = dealer.subMerchantCodeMC {
        //            mcContext.TransactionMerchantCode   = subMerchant
        //        }
        
        mcContext.merchantCode              = "0000073" //MC_merchantID
        mcContext.apiLogin                  = "pRRXKOl8ikMmt9u1" //MC_accesKeyID
        mcContext.apiKey                    = "VErethUtraQuxas57wuMuquprADrAHAb" //MC_secretAccessKey
        
        
        return mcContext
    }
    
    func loadParameters() -> Bool {
        // variables for MC context
        let transactionAutogenerate = self.getTransactionOrderNumber()
        self.transactionOrderNumber = self.getTransactionOrderNumber()
        
        self.transactionDate = self.getpaymentDate(stringFromDateFormat: "yyyyMMdd")
        self.transactionTime = self.getpaymentDate(stringFromDateFormat: "HHmmss")
        
        do {
            let mcContext = try self.loadMerchantVariables()
            //            mcContext.MerchantCode              = "8004749" // Test
            //            mcContext.MerchantCode              = "4018478" // Prod
            mcContext.transactionOrderNumber    = self.transactionOrderNumber
            mcContext.transactionCurrency       = .PEN
            mcContext.transactionAmount         = self.transactionAmount
            mcContext.transactionLanguage       = .SPANISH
            mcContext.buyerTokenId              = self.tokenDevice
            
            /*
            if let kindDoc = DocumentoType.getType(self.cardholder.documentType) {
                switch kindDoc {
                case .DNI:
                    self.cardholderKindDoc = "DNI"
                case .CarneExtranjeria:
                    self.cardholderKindDoc = "CE"
                case .Pasaporte:
                    self.cardholderKindDoc = "PASAPORTE"
                }
            }
            */
            
            mcContext.transactionSignature      = self.getSignature(mcContext: mcContext)
            mcContext.buyerMerchantId           = self.userDeviceCode
            mcContext.buyerDocType              = "DNI" //self.cardholderKindDoc
            mcContext.buyerDocNumber            = self.cardholder.documentNumber
            mcContext.buyerFirstName            = self.cardholderName
            mcContext.buyerLastName             = self.cardholderLastName
            mcContext.buyerEmail                = self.cardholderEmail
            mcContext.transactionAutogenerate   = transactionAutogenerate
            mcContext.transactionAutenticationDate  = self.transactionDate
            mcContext.transactionAutenticationTime  = self.transactionTime
            mcContext.event                     = self.isTransactionRegister ? .REGISTRAR_TARJETA : .AUTORIZACION
            mcContext.entorno                   = kGlobalEnvironmentGateway ? .DEVELOPMENT : .PRODUCTION
            self.gatewayContext                 = mcContext
        } catch {
            return false
        }
        
        return true
    }
    
    func callPayment(view: UIViewController) {
        self.viewControllerContainer = view
        
        if let mcContext = self.gatewayContext {
            let contenedor = PayerInitVC(context: mcContext)
            contenedor?.delegate = self
            
            contenedor?.modalPresentationStyle = .custom
            contenedor?.transitioningDelegate = self
            
            print("MC payment gateway starting with detail")
            view.present(contenedor!, animated: true, completion: nil)
        }
    }
    
    
    //**************************************************
    // MARK: - Override Public Methods
    //**************************************************

}

extension PMPMCTransaction: UIViewControllerTransitioningDelegate, PMPPaymentDelegate {
    //**************************************************
    // MARK: - Properties
    //**************************************************
    
    //**************************************************
    // MARK: - Constructors
    //**************************************************
    
    //**************************************************
    // MARK: - Internal Methods
    //**************************************************
    
    func paymentDidCancel() {
        
        print("MC gateway was cancelled by the user")
        
        if let view = self.viewControllerContainer
        {
            view.dismiss(animated: true, completion: nil)
        }
    }
    
    func paymendDidComplete(_ paymentInfo: PMPPaymentInfo!) {
        
        //print("MC gateway was successfully ended \(paymentInfo.responseMessage)")
        
        if let view = self.viewControllerContainer {
            
            view.dismiss(animated: true, completion: {
                (action) in
                
                var title = "Alerta"
                var message = "Su transacción no se completo."
                
                var responseIndicator = MCErrorType.ALFANUMERICO
                
                if let respCode = paymentInfo.responseCode, let rc = Int(respCode) {
                    if rc == 0 {
                        responseIndicator = .SIN_ERROR
                    } else {
                        responseIndicator = .NUMERICO
                    }
                }
                
                
                if responseIndicator == .SIN_ERROR {
                    
                    print("(MC) --> RC: \(paymentInfo.responseCode ?? "") -- RM: \(paymentInfo.responseMessage ?? "")")
                    
                    
                } else {
                    var errorMessage = "MC no tuvo respuesta"
                    print("(MC) has an error with detail: \(paymentInfo.responseCode ?? "") -- RM: \(paymentInfo.responseMessage ?? "")")
                    
                }
            })
        }
    }
    
    
    internal func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PMPPaymentPresentAnimationController()
    }
    
    internal func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PMPPaymentDismissAnimationController()
    }
    
    //**************************************************
    // MARK: - Private Methods
    //**************************************************
    
    //**************************************************
    // MARK: - Self Public Methods
    //**************************************************
    
    //**************************************************
    // MARK: - Override Public Methods
    //**************************************************
    
}


extension PMPConfigurationContext {
    func detailForLog() -> String{
        let contextMC = "TransactionOrderNumber: \(transactionOrderNumber)\nTransactionAmount: \(transactionAmount)\nTransactionMerchantCode: \(transactionMerchantCode)\nTransactionSignature: \(transactionSignature)\nBuyerDocType: \(buyerDocType)\nBuyerDocNumber: \(buyerDocNumber)\nBuyerFirstName: \(buyerFirstName)\nBuyerLastName: \(buyerLastName)\nBuyerEmail: \(buyerEmail)\nTransactionAutogenerate: \(transactionAutogenerate)\nTransactionAutenticationDate: \(transactionAutenticationDate)\nTransactionAutenticationTime: \(transactionAutenticationTime)"
        
        return contextMC
    }
}
