//
//  PMPUser.swift
//  LibraryIntegrationPMPiOS
//
//  Created by APPLE on 17/09/17.
//  Copyright © 2017 LUIS MARCA ZAPATA. All rights reserved.
//

import UIKit


//
// MARK: - Class -
//
//**********************************************************************************************************

enum documentType {
    case DNI, CarneExtranjeria, Pasaporte
}


class PMPUser: NSObject {

    //**************************************************
    // MARK: - Properties
    //**************************************************
    
    var email: String
    var name: String
    var firstName: String
    var documentNumber: String
    var amount: Double
    
    init(email: String,
         name: String,
         firstName: String,
         documentNumber: String,
         amount: Double) {
        self.email = email
        self.name = name
        self.firstName = firstName
        self.documentNumber = documentNumber
        self.amount = amount
    }
}
